/**
 * Copyright (c) 2022-2024
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *    Abrar (https://gitlab.com/s96abrar)
 *    rahmanshaber (https://gitlab.com/rahmanshaber)
 *
 * DFL::Storage provides a C++ API for accessing devices and
 * partitions that are exposed by udisks2 via dbus.
 **/

#pragma once

#include <QtDBus>
#include <QString>
#include <QVariantMap>
#include <QStringList>

namespace DFL {
    namespace Storage {
        class Manager;
        class Device;
        class Volume;

        typedef QList<Device *>                              Devices;
        typedef QList<Volume *>                              Volumes;

        /** This map contains all the properties of a device or a block */
        typedef QMap<QString, QVariantMap>                   UDisksPropertiesMap;

        /** this map contains a map of all the node (device/volume/manager) paths and their properties */
        typedef QMap<QDBusObjectPath, UDisksPropertiesMap>   UDisksNodeMap;
    }
}

class DFL::Storage::Manager : public QObject {
    Q_OBJECT

    public:
        explicit Manager( QObject *parent = nullptr );
        ~Manager();

        void startMonitoring();
        void stopMonitoring();

        QStringList getAllDevices() const;
        QStringList getAllVolumes() const;

    signals:
        void deviceAdded( const QString& devicePath );
        void deviceRemoved( const QString& devicePath );
        void volumeAdded( const QString& volumePath );
        void volumeRemoved( const QString& volumePath );

    private slots:
        void handleDeviceAdded( const QDBusObjectPath& objectPath );
        void handleDeviceRemoved( const QDBusObjectPath& objectPath );

    private:
        QDBusInterface *udisksInterface;
};

class DFL::Storage::Device {
    public:
        explicit Device( const QString& objectPath );
        ~Device();

        QStringList listVolumes() const;
        QVariantMap listProperties() const;
        bool powerOff() const;
        bool eject() const;

        bool isRemovable() const;

    private:
        QString devicePath;
        QDBusInterface *deviceInterface;
};

class DFL::Storage::Volume {
    public:
        explicit Volume( const QString& objectPath );
        ~Volume();

        QVariantMap listProperties() const;
        bool mount( const QVariantMap& options );
        bool unmount( const QVariantMap& options );
        QStringList listMountPoints() const;
        QString getDevicePath() const;

    private:
        QString volumePath;
        QDBusInterface *filesystemInterface;
        QDBusInterface *partitionInterface;
        QDBusInterface *blockInterface;
};
