/**
 * Copyright (c) 2022-2024
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *    Abrar (https://gitlab.com/s96abrar)
 *    rahmanshaber (https://gitlab.com/rahmanshaber)
 *
 * DFL::Storage provides a C++ API for accessing devices and
 * partitions that are exposed by udisks2 via dbus.
 **/

#pragma once

#include <QtDBus>
#include <QString>

namespace DFL {
    namespace Storage {
        class Manager;
        class Device;
        class Volume;

        typedef QList<Device *>                              Devices;
        typedef QList<Volume *>                              Volumes;

        /** This map contains all the properties of a device or a block */
        typedef QMap<QString, QVariantMap>                   UDisksPropertiesMap;

        /** this map contains a map of all the node (device/volume/manager) paths and their properties */
        typedef QMap<QDBusObjectPath, UDisksPropertiesMap>   UDisksNodeMap;
    }
}

class DFL::Storage::Manager : public QObject {
    Q_OBJECT

    public:
        /** Initialization */
        Manager();

        /** List all the available devices */
        DFL::Storage::Devices devices();

        /**
         * All the available blocks: will have containers and whole disks and partitions,
         * including encrypted volumes
         */
        DFL::Storage::Volumes allVolumes();

        /**
         * All the volumes that have a valid mount point
         * Use this to list the drives in a side view (for example).
         */
        DFL::Storage::Volumes allMountedVolumes();

        /** Get the device this volume belonga to */
        DFL::Storage::Device * deviceForVolume( DFL::Storage::Volume * );

        /** Device for a path */
        DFL::Storage::Volume * volumeForFilePath( QString );

    private Q_SLOTS:
        /* Handle adding of new drives and partitions */
        void interfacesAdded( const QDBusObjectPath&, const QVariantMap& );

        /* Handle removing of new drives and partitions */
        void interfacesRemoved( const QDBusObjectPath&, const QStringList& );

    private:
        /* Rescan all the available devices */
        void rescanStorage();

        /* Map of all available devices */
        QMap<QString, DFL::Storage::Device *> mDevices;

        /* Map of all available volumes */
        QMap<QString, DFL::Storage::Volume *> mVolumes;

    Q_SIGNALS:
        /* Something was added to /org/freedesktop/UDisks2/drives */
        void deviceAdded( DFL::Storage::Device * );

        /* Something was added to /org/freedesktop/UDisks2/drives */
        void deviceRemoved( DFL::Storage::Device * );

        /* Something was added to /etc/mtab (mountPoint) */
        void volumeAdded( DFL::Storage::Volume * );

        /* Something was removed from /etc/mtab (mountPoint) */
        void volumeRemoved( DFL::Storage::Volume * );

        /** Reload the GUI: Make the process seem atomic. */
        void reload();
};

class DFL::Storage::Device {
    public:
        Device();
        Device( QString, UDisksPropertiesMap );

        bool isValid();

        /**
         * All the available blocks: will have containers and whole disks and partitions,
         * including encrypted volumes
         */
        DFL::Storage::Volumes allVolumes();

        /**
         * All the volumes that have a valid mount point
         * Use this to list the drives in a side view (for example).
         */
        DFL::Storage::Volumes allMountedVolumes();

        QString label();
        QString path();
        QString id();
        bool isRemovable();
        bool isOptical();
        qint64 size();
        int rotationRate();
        QString seat();

        QVariant property( QString key );

    private:
        /** Populate the volumes */
        void addVolume( DFL::Storage::Volume * );
        void removeVolume( DFL::Storage::Volume * );

        DFL::Storage::Volumes mVolumes;

        QString mDriveId;

        QString mLabel    = QString();
        QString mPath     = QString();
        QString mId       = QString();
        bool mIsRemovable = false;
        bool mIsOptical   = false;
        qint64 mSize      = qint64();
        int mRotationRate = 0;
        QString mSeat     = QString();

        bool mIsValid = false;

        QVariantMap mDriveProps;

        friend class DFL::Storage::Manager;
};

class DFL::Storage::Volume {
    public:
        Volume();

        /** UDisks ID */
        Volume( QString, UDisksPropertiesMap );

        /** Is this object valid */
        bool isValid();

        /**
         * Can this volume be mounted?
         *  /dev/nvme0n1   -> return false (whole device)
         *  /dev/nvme0n1p1 -> return true  (partition)
         *  /dev/nvme0n1p4 -> return false (partition, but swap)
         */
        bool isMountable();

        /** Is this volume mounted or not */
        bool isMounted();

        /**
         * Is this a partition or a whole device?
         * ex: /dev/nvme0n1   -> whole device, but a block device (return false)
         *     /dev/nvme0n1p1 -> partition, also a block device (return true)
         */
        bool isPartition();

        /** Is this a swap partition? */
        bool isSwap();

        /**
         * Label of this volume
         * Return "Label" property if set. Else, if mounted, returns the
         * basename of the mount moint. If not mounted, return the "id".
         */
        QString label();

        /** UDisks path: f.e. /org/freedesktop/UDisks2/block_devices/nvme0n1p1 */
        QString path();

        /** /dev/<path> */
        QString devicePath();

        /** UDisks drive path, if it's real partition */
        QString drive();

        /**
         * Mount point, if mounted.
         */
        QStringList mountPoints();

        /** Filesystem of the volume */
        QString fileSystem();

        bool isOptical();
        bool isRemovable();

        qint64 availableSize();
        qint64 totalSize();

        /** For non-udisks volumes this is invalid */
        QStringList availableInterfaces();
        QStringList availableProperties( QString interface );
        QVariant property( QString interface, QString key );

        /** Attempt to mount */
        bool mount();

        /**
         * Attempt to unmount
         * If it's a udisks device, use udisks.
         * If not, try to use fusermount -u <mount-point>
         */
        bool unmount();

    private:
        void updateMountPoints();

        QString mLabel;
        QString mPath;

        QString mDevicePath;
        QStringList mMountPoints;

        qint64 mAvailableSize = 0;

        /** Various properties of this volume */
        UDisksPropertiesMap mVolumeProps;

        friend class DFL::Storage::Manager;
};
