/**
 * Copyright (c) 2022-2024
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *    Abrar (https://gitlab.com/s96abrar)
 *    rahmanshaber (https://gitlab.com/rahmanshaber)
 *
 * DFL::Storage provides a C++ API for accessing devices and
 * partitions that are exposed by udisks2 via dbus.
 **/

#include <QFileInfo>
#include <QIODevice>
#include <QXmlStreamReader>

#include "DFStorage2.hpp"

#include <sys/statvfs.h>
#include <mntent.h>

QStringList virtualFS = QStringList()
                        << "sysfs" << "cgroup" << "cgroup2" << "proc" << "devtmpfs" << "devpts"
                        << "tmpfs" << "securityfs" << "pstore" << "autofs" << "mqueue"
                        << "debugfs" << "hugetlbfs" << "fusectl" << "fuse.gvfsd-fuse"
                        << "binfmt_misc" << "configfs" << "bpf" << "fuse.portal" << "fuse.cryfs";

const QStringList systemFS = { "efivarfs" };

// Manager Implementation
DFL::Storage::Manager::Manager( QObject *parent ) : QObject( parent ) {
    udisksInterface = new QDBusInterface(
        "org.freedesktop.UDisks2",
        "/org/freedesktop/UDisks2",
        "org.freedesktop.DBus.ObjectManager",
        QDBusConnection::systemBus(),
        this
    );

    connect(
        QDBusConnection::systemBus().interface(), &QDBusConnectionInterface::serviceOwnerChanged, this, [ this ](const QString& serviceName, const QString&, const QString& newOwner) {
            if ( (serviceName == "org.freedesktop.UDisks2") && !newOwner.isEmpty() ) {
                startMonitoring();
            }
        }
    );
}


DFL::Storage::Manager::~Manager() {
    stopMonitoring();
    delete udisksInterface;
}


void DFL::Storage::Manager::startMonitoring() {
    if ( !udisksInterface->isValid() ) {
        qWarning() << "UDisks2 interface is not valid.";
        return;
    }

    QDBusPendingCall asyncCall = udisksInterface->asyncCall( "GetManagedObjects" );
    auto             watcher   = new QDBusPendingCallWatcher( asyncCall, this );

    connect(
        watcher, &QDBusPendingCallWatcher::finished, this, [ this ](QDBusPendingCallWatcher *watcher) {
            QDBusPendingReply<QVariantMap> reply = *watcher;

            if ( reply.isError() ) {
                qWarning() << "Error fetching managed objects:" << reply.error().message();
            }

            else {
                QVariantMap managedObjects = reply.value();
                for (const auto& path : managedObjects.keys() ) {
                    if ( path.contains( "/drives/" ) ) {
                        emit deviceAdded( path );
                    }
                    else if ( path.contains( "/block_devices/" ) ) {
                        emit volumeAdded( path );
                    }
                }
            }

            watcher->deleteLater();
        }
    );

    QDBusConnection::systemBus().connect(
        "org.freedesktop.UDisks2", "/org/freedesktop/UDisks2",
        "org.freedesktop.DBus.ObjectManager", "InterfacesAdded",
        this, SLOT(handleDeviceAdded(QDBusObjectPath)) );

    QDBusConnection::systemBus().connect(
        "org.freedesktop.UDisks2", "/org/freedesktop/UDisks2",
        "org.freedesktop.DBus.ObjectManager", "InterfacesRemoved",
        this, SLOT(handleDeviceRemoved(QDBusObjectPath)) );
}


void DFL::Storage::Manager::stopMonitoring() {
    QDBusConnection::systemBus().disconnect(
        "org.freedesktop.UDisks2", "/org/freedesktop/UDisks2",
        "org.freedesktop.DBus.ObjectManager", "InterfacesAdded",
        this, SLOT(handleDeviceAdded(QDBusObjectPath)) );

    QDBusConnection::systemBus().disconnect(
        "org.freedesktop.UDisks2", "/org/freedesktop/UDisks2",
        "org.freedesktop.DBus.ObjectManager", "InterfacesRemoved",
        this, SLOT(handleDeviceRemoved(QDBusObjectPath)) );
}


QStringList DFL::Storage::Manager::getAllDevices() const {
    QStringList devices;
    QDBusPendingReply<QVariantMap> reply = udisksInterface->call( "GetManagedObjects" );

    if ( reply.isValid() ) {
        QVariantMap managedObjects = reply.value();
        for (const auto& path : managedObjects.keys() ) {
            if ( path.contains( "/drives/" ) ) {
                devices.append( path );
            }
        }
    }

    return devices;
}


QStringList DFL::Storage::Manager::getAllVolumes() const {
    QStringList volumes;
    QDBusPendingReply<QVariantMap> reply = udisksInterface->call( "GetManagedObjects" );

    if ( reply.isValid() ) {
        QVariantMap managedObjects = reply.value();
        for (const auto& path : managedObjects.keys() ) {
            if ( path.contains( "/block_devices/" ) ) {
                volumes.append( path );
            }
        }
    }

    return volumes;
}


void DFL::Storage::Manager::handleDeviceAdded( const QDBusObjectPath& objectPath ) {
    if ( objectPath.path().contains( "/drives/" ) ) {
        emit deviceAdded( objectPath.path() );
    }
    else if ( objectPath.path().contains( "/block_devices/" ) ) {
        emit volumeAdded( objectPath.path() );
    }
}


void DFL::Storage::Manager::handleDeviceRemoved( const QDBusObjectPath& objectPath ) {
    if ( objectPath.path().contains( "/drives/" ) ) {
        emit deviceRemoved( objectPath.path() );
    }
    else if ( objectPath.path().contains( "/block_devices/" ) ) {
        emit volumeRemoved( objectPath.path() );
    }
}


// Device Implementation
DFL::Storage::Device::Device( const QString& objectPath ) : devicePath( objectPath ) {
    deviceInterface = new QDBusInterface(
        "org.freedesktop.UDisks2",
        objectPath,
        "org.freedesktop.UDisks2.Drive",
        QDBusConnection::systemBus(),
        nullptr
    );
}


DFL::Storage::Device::~Device() {
    delete deviceInterface;
}


QStringList DFL::Storage::Device::listVolumes() const {
    QDBusPendingReply<QVariant> reply = deviceInterface->asyncCall( "Get", "org.freedesktop.UDisks2.Block", "Devices" );

    reply.waitForFinished();

    if ( reply.isValid() ) {
        return reply.value().toStringList();
    }

    return {};
}


QVariantMap DFL::Storage::Device::listProperties() const {
    QDBusPendingReply<QVariantMap> reply = deviceInterface->asyncCall( "GetAll", "org.freedesktop.UDisks2.Drive" );

    reply.waitForFinished();
    return reply.isValid() ? reply.value() : QVariantMap();
}


bool DFL::Storage::Device::powerOff() const {
    QDBusPendingReply<void> reply = deviceInterface->asyncCall( "PowerOff" );

    reply.waitForFinished();
    return reply.isValid();
}


bool DFL::Storage::Device::eject() const {
    QDBusPendingReply<void> reply = deviceInterface->asyncCall( "Eject" );

    reply.waitForFinished();
    return reply.isValid();
}


bool DFL::Storage::isRemovable() const {
    // Query the Removable property
    QDBusPendingReply<bool> removableReply = deviceInterface->asyncCall( "Get", "org.freedesktop.UDisks2.Drive", "Removable" );

    removableReply.waitForFinished();
    bool isRemovable = removableReply.isValid() && removableReply.value();

    // If not removable, check MediaCompatibility for CD/DVD/BluRay drives
    if ( !isRemovable ) {
        QDBusPendingReply<QStringList> mediaReply = deviceInterface->asyncCall( "Get", "org.freedesktop.UDisks2.Drive", "MediaCompatibility" );
        mediaReply.waitForFinished();

        if ( mediaReply.isValid() ) {
            QStringList mediaTypes = mediaReply.value();
            for (const QString& type : mediaTypes) {
                bool optical = type.contains( "optical", Qt::CaseInsensitive );
                bool cd      = type.contains( "cd", Qt::CaseInsensitive );
                bool dvd     = type.contains( "dvd", Qt::CaseInsensitive );
                bool bluray  = type.contains( "blu-ray", Qt::CaseInsensitive );

                if ( optical || cd || dvd || bluray ) {
                    return true;
                }
            }
        }
    }

    return isRemovable;
}


// Volume Implementation
DFL::Storage::Volume::Volume( const QString& objectPath ) {
    volumePath = objectPath;

    filesystemInterface = new QDBusInterface(
        "org.freedesktop.UDisks2", objectPath,
        "org.freedesktop.UDisks2.Filesystem",
        QDBusConnection::systemBus(),
        nullptr
    );

    partitionInterface = new QDBusInterface(
        "org.freedesktop.UDisks2", objectPath,
        "org.freedesktop.UDisks2.Partition",
        QDBusConnection::systemBus(),
        nullptr
    );

    blockInterface = new QDBusInterface(
        "org.freedesktop.UDisks2", objectPath,
        "org.freedesktop.UDisks2.Block",
        QDBusConnection::systemBus(),
        nullptr
    );
}


DFL::Storage::Volume::~Volume() {
    delete filesystemInterface;
    delete partitionInterface;
    delete blockInterface;
}


QVariantMap DFL::Storage::Volume::listProperties() const {
    QDBusPendingReply<QVariantMap> reply = filesystemInterface->asyncCall( "GetAll", "org.freedesktop.UDisks2.Filesystem" );

    reply.waitForFinished();
    return reply.isValid() ? reply.value() : QVariantMap();
}


bool DFL::Storage::Volume::mount( const QVariantMap& options ) {
    QDBusPendingReply<void> reply = filesystemInterface->asyncCall( "Mount", options );

    reply.waitForFinished();
    return reply.isValid();
}


bool DFL::Storage::Volume::unmount( const QVariantMap& options ) {
    QDBusPendingReply<void> reply = filesystemInterface->asyncCall( "Unmount", options );

    reply.waitForFinished();
    return reply.isValid();
}


QStringList DFL::Storage::Volume::listMountPoints() const {
    QDBusPendingReply<QStringList> reply = filesystemInterface->asyncCall( "Get", "org.freedesktop.UDisks2.Filesystem", "MountPoints" );

    reply.waitForFinished();
    return reply.isValid() ? reply.value() : QStringList();
}


QString DFL::Storage::Volume::getDevicePath() const {
    QDBusPendingReply<QString> reply = blockInterface->asyncCall( "Get", "org.freedesktop.UDisks2.Block", "Drive" );

    reply.waitForFinished();
    return reply.isValid() ? reply.value() : QString();
}
