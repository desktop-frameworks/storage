/**
 * Copyright (c) 2022-2024
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *    Abrar (https://gitlab.com/s96abrar)
 *    rahmanshaber (https://gitlab.com/rahmanshaber)
 *
 * DFL::Storage provides a C++ API for accessing devices and
 * partitions that are exposed by udisks2 via dbus.
 **/

#include <QFileInfo>
#include <QIODevice>
#include <QXmlStreamReader>

#include <QDBusMessage>
#include <QDBusInterface>
#include <QDBusConnection>
#include <QVariant>
#include <QVariantMap>
#include <QVariantList>

#include "DFStorage.hpp"

#include <sys/statvfs.h>
#include <mntent.h>

#define UDISKS_SERVICE     "org.freedesktop.UDisks2"
#define UDISKS_IFACE       "org.freedesktop.UDisks2"
#define UDISKS_PATH        "/org/freedesktop/UDisks2"
#define DBUS_OBJMGR        "org.freedesktop.DBus.ObjectManager"
#define BLOCK_PATH         "/org/freedesktop/UDisks2/block_devices"
#define DRIVE_PATH         "/org/freedesktop/UDisks2/drives"
#define DBUS_INTROSPECT    "org.freedesktop.DBus.Introspectable"

QStringList virtualFS = QStringList()
                        << "sysfs" << "cgroup" << "cgroup2" << "proc" << "devtmpfs" << "devpts"
                        << "tmpfs" << "securityfs" << "pstore" << "autofs" << "mqueue"
                        << "debugfs" << "hugetlbfs" << "fusectl" << "fuse.gvfsd-fuse"
                        << "binfmt_misc" << "configfs" << "bpf" << "fuse.portal" << "fuse.cryfs";

const QStringList systemFS = { "efivarfs" };

#include <QDBusArgument>
#include <QDBusMetaType>
#include <QDebug>

/**
 * DFL::Storage::Manager - Singleton class that manages all the DFL::Storage:: volumes
 */

DFL::Storage::Manager::Manager() : QObject() {
    QDBusConnection::systemBus().connect(
        "org.freedesktop.UDisks2",
        "/org/freedesktop/UDisks2",
        "org.freedesktop.DBus.ObjectManager",
        "InterfacesAdded",
        this,
        SLOT( interfacesAdded( QDBusObjectPath, QVariantMap ) )
    );

    QDBusConnection::systemBus().connect(
        "org.freedesktop.UDisks2",
        "/org/freedesktop/UDisks2",
        "org.freedesktop.DBus.ObjectManager",
        "InterfacesRemoved",
        this,
        SLOT( interfacesRemoved( QDBusObjectPath, QStringList ) )
    );

    rescanStorage();
}


DFL::Storage::Devices DFL::Storage::Manager::devices() {
    return mDevices.values();
}


DFL::Storage::Volumes DFL::Storage::Manager::allVolumes() {
    return mVolumes.values();
}


DFL::Storage::Volumes DFL::Storage::Manager::allMountedVolumes() {
    DFL::Storage::Volumes mountedVols;

    for ( DFL::Storage::Volume *vol: mVolumes ) {
        if ( vol->isMounted() ) {
            mountedVols << vol;
        }
    }

    return mountedVols;
}


DFL::Storage::Device *DFL::Storage::Manager::deviceForVolume( DFL::Storage::Volume *vol ) {
    QString drive = vol->drive();

    if ( mDevices.contains( drive ) ) {
        return mDevices[ drive ];
    }

    qCritical() << "Unable to find the device" << drive << "for volume" << vol->devicePath() << vol->path();

    return nullptr;
}


DFL::Storage::Volume * DFL::Storage::Manager::volumeForFilePath( QString path ) {
    rescanStorage();

    QString              largest;
    DFL::Storage::Volume *blk = nullptr;

    for ( DFL::Storage::Volume *block: allVolumes() ) {
        /** Does not contain a file system - we ignore it. */
        if ( block->fileSystem().length() == 0 ) {
            continue;
        }

        QString mtpt( block->mountPoints().at( 0 ) );

        if ( path.contains( mtpt ) and (mtpt.length() > largest.length() ) ) {
            largest = mtpt;
            blk     = block;
        }
    }

    return blk;
}


void DFL::Storage::Manager::rescanStorage() {
    QDBusInterface iface( UDISKS_SERVICE, UDISKS_PATH, DBUS_OBJMGR, QDBusConnection::systemBus() );

    if ( iface.isValid() == false ) {
        return;
    }

    mDevices.clear();
    mVolumes.clear();

    // Call the GetManagedObjects method
    QDBusMessage message = iface.call( "GetManagedObjects" );

    // Check if the call was successful
    if ( message.type() == QDBusMessage::ReplyMessage ) {
        QDBusArgument arg = message.arguments().at( 0 ).value<QDBusArgument>();
        UDisksNodeMap map;
        arg >> map;

        for ( QDBusObjectPath key: map.keys() ) {
            /** This is a Drive */
            UDisksPropertiesMap props    = map[ key ];
            QString             dbusPath = key.path();

            if ( dbusPath.contains( "/drives/" ) ) {
                mDevices[ dbusPath ] = new DFL::Storage::Device( dbusPath, props );
            }

            /** This is a Volume */
            else if ( dbusPath.contains( "/block_devices/" ) ) {
                mVolumes[ dbusPath ] = new DFL::Storage::Volume( dbusPath, props );
            }

            else {
                qDebug() << "Ignoring" << dbusPath;
            }
        }
    }

    for ( DFL::Storage::Volume *vol: mVolumes ) {
        /** Get the drive this volume belongs to */
        QString drive = vol->drive();

        if ( mDevices.contains( drive ) ) {
            mDevices[ drive ]->addVolume( vol );
        }

        else {
            qCritical() << "Volume without a drive" << vol->path() << vol->devicePath() << drive;
        }
    }
}


void DFL::Storage::Manager::interfacesAdded( const QDBusObjectPath&, const QVariantMap& ) {
    auto mDevicesOld = mDevices;
    auto mVolumesOld = mVolumes;

    rescanStorage();

    int delta = 0;

    for ( DFL::Storage::Device *dev: mDevices ) {
        if ( mDevicesOld.contains( dev->path() ) == false ) {
            emit deviceAdded( dev );
            delta++;
        }
    }

    for ( DFL::Storage::Volume *vol: mVolumes ) {
        if ( mVolumesOld.contains( vol->path() ) == false ) {
            emit volumeAdded( vol );
            delta++;
        }
    }

    if ( delta ) {
        emit reload();
    }
}


void DFL::Storage::Manager::interfacesRemoved( const QDBusObjectPath&, const QStringList& ) {
    auto mDevicesOld = mDevices;
    auto mVolumesOld = mVolumes;

    rescanStorage();

    int delta = 0;

    for ( DFL::Storage::Device *dev: mDevicesOld ) {
        if ( mDevices.contains( dev->path() ) == false ) {
            emit deviceRemoved( dev );
            delta++;
        }
    }

    for ( DFL::Storage::Volume *vol: mVolumesOld ) {
        if ( mVolumes.contains( vol->path() ) == false ) {
            emit volumeRemoved( vol );
            delta++;
        }
    }

    if ( delta ) {
        emit reload();
    }
}


/**
 * DFL::Storage::Device - Class to access the physical devices
 */

DFL::Storage::Device::Device() {
    mIsValid      = false;
    mPath         = QString();
    mId           = QString();
    mIsRemovable  = false;
    mIsOptical    = false;
    mSize         = 0;
    mRotationRate = 0;
    mSeat         = QString();
}


DFL::Storage::Device::Device( QString drive, DFL::Storage::UDisksPropertiesMap props ) {
    mDriveProps = props[ "org.freedesktop.UDisks2.Drive" ];

    mLabel += mDriveProps[ "Vendor" ].toString();
    mLabel += " " + mDriveProps[ "Model" ].toString();
    mLabel  = mLabel.trimmed();

    mPath = drive;

    mId           = property( "Id" ).toString();
    mIsRemovable  = property( "Removable" ).toBool();
    mIsOptical    = (property( "MediaCompatibility" ).toStringList().filter( "optical" ).length() > 0);
    mSize         = property( "Size" ).toULongLong();
    mRotationRate = property( "RotationRate" ).toInt();
    mSeat         = property( "Seat" ).toString();

    mIsValid = (mId.isEmpty() ? false : true);
}


bool DFL::Storage::Device::isValid() {
    return true;
}


DFL::Storage::Volumes DFL::Storage::Device::allVolumes() {
    return mVolumes;
}


DFL::Storage::Volumes DFL::Storage::Device::allMountedVolumes() {
    DFL::Storage::Volumes mountedVols;

    for ( DFL::Storage::Volume *vol: mVolumes ) {
        if ( vol->isMounted() ) {
            mountedVols << vol;
        }
    }

    return mountedVols;
}


QString DFL::Storage::Device::label() {
    return mLabel;
}


QString DFL::Storage::Device::path() {
    return mPath;
}


QString DFL::Storage::Device::id() {
    return mDriveProps[ "Id" ].toString();
}


bool DFL::Storage::Device::isRemovable() {
    return mDriveProps[ "Removable" ].toBool();
}


bool DFL::Storage::Device::isOptical() {
    return (mDriveProps[ "MediaCompatibility" ].toStringList().filter( "optical" ).length() > 0);
}


qint64 DFL::Storage::Device::size() {
    return mDriveProps[ "Size" ].toULongLong();
}


int DFL::Storage::Device::rotationRate() {
    return mDriveProps[ "RotationRate" ].toInt();
}


QString DFL::Storage::Device::seat() {
    return mDriveProps[ "Seat" ].toString();
}


QVariant DFL::Storage::Device::property( QString key ) {
    return mDriveProps[ key ];
}


void DFL::Storage::Device::addVolume( DFL::Storage::Volume *vol ) {
    mVolumes << vol;
}


void DFL::Storage::Device::removeVolume( DFL::Storage::Volume *vol ) {
    mVolumes.removeAll( vol );
}


/**
 * DFL::Storage::Volume - Class to access a block device (physical or loop or any such)
 */

DFL::Storage::Volume::Volume() {
    mLabel         = QString();
    mPath          = QString();
    mDevicePath    = QString();
    mAvailableSize = 0;
}


DFL::Storage::Volume::Volume( QString path, DFL::Storage::UDisksPropertiesMap props ) {
    mPath = path;

    mVolumeProps = props;

    mDevicePath = mVolumeProps[ "org.freedesktop.UDisks2.Block" ][ "Device" ].toString();
    mDevicePath.remove( '\u0000' );

    updateMountPoints();

    if ( mountPoints().length() ) {
        struct statvfs buf;

        if ( statvfs( mountPoints().at( 0 ).toUtf8().constData(), &buf ) == 0 ) {
            mAvailableSize = buf.f_bavail * buf.f_frsize;
        }
    }
}


bool DFL::Storage::Volume::isValid() {
    return QFileInfo( mDevicePath ).exists();
}


QString DFL::Storage::Volume::label() {
    return mVolumeProps[ "org.freedesktop.UDisks2.Block" ][ "IdLabel" ].toString();
}


QString DFL::Storage::Volume::path() {
    return mPath;
}


QString DFL::Storage::Volume::devicePath() {
    return mDevicePath;
}


QString DFL::Storage::Volume::drive() {
    return mVolumeProps[ "org.freedesktop.UDisks2.Block" ][ "Drive" ].value<QDBusObjectPath>().path();
}


QStringList DFL::Storage::Volume::mountPoints() {
    return mMountPoints;
}


QString DFL::Storage::Volume::fileSystem() {
    return mVolumeProps[ "org.freedesktop.UDisks2.Block" ][ "IdType" ].toString();
}


bool DFL::Storage::Volume::isOptical() {
    return (mVolumeProps[ "org.freedesktop.UDisks2.Block" ][ "MediaCompatibility" ].toStringList().filter( "optical" ).length() > 0);
}


bool DFL::Storage::Volume::isRemovable() {
    return mVolumeProps[ "org.freedesktop.UDisks2.Block" ][ "Removable" ].toBool();
}


qint64 DFL::Storage::Volume::totalSize() {
    return mVolumeProps[ "org.freedesktop.UDisks2.Block" ][ "Size" ].toULongLong();
}


qint64 DFL::Storage::Volume::availableSize() {
    return mAvailableSize;
}


QStringList DFL::Storage::Volume::availableInterfaces() {
    return mVolumeProps.keys();
}


QStringList DFL::Storage::Volume::availableProperties( QString interface ) {
    return mVolumeProps[ interface ].keys();
}


QVariant DFL::Storage::Volume::property( QString interface, QString key ) {
    if ( mVolumeProps.contains( interface ) && mVolumeProps[ interface ].contains( key ) ) {
        return mVolumeProps[ interface ][ key ];
    }

    return QVariant();
}


bool DFL::Storage::Volume::mount() {
    QProcess proc;

    proc.start( QString( "udisksctl" ), QStringList() << "mount" << "-b" << mDevicePath );
    proc.waitForFinished();

    updateMountPoints();

    return (proc.exitCode() > 0 ? false : true);
}


bool DFL::Storage::Volume::unmount() {
    QProcess proc;

    proc.start( QString( "udisksctl" ), QStringList() << "unmount" << "-b" << mDevicePath );
    proc.waitForFinished();

    updateMountPoints();

    return (proc.exitCode() > 0 ? false : true);
}


bool DFL::Storage::Volume::isMountable() {
    /** Should be a partition, should not be swap and should contain a FS */
    return availableInterfaces().contains( "org.freedesktop.UDisks2.Filesystem" ) && isPartition() && (isSwap() == false);
}


bool DFL::Storage::Volume::isMounted() {
    return (mMountPoints.length() > 0);
}


bool DFL::Storage::Volume::isPartition() {
    return availableInterfaces().contains( "org.freedesktop.UDisks2.Partition" );
}


bool DFL::Storage::Volume::isSwap() {
    return availableInterfaces().contains( "org.freedesktop.UDisks2.Swapspace" );
}


void DFL::Storage::Volume::updateMountPoints() {
    mMountPoints.clear();

    QDBusArgument  mtPtsDBus = mVolumeProps[ "org.freedesktop.UDisks2.Filesystem" ][ "MountPoints" ].value<QDBusArgument>();
    QByteArrayList mtPts;
    mtPtsDBus >> mtPts;

    for ( QByteArray mtPt: mtPts ) {
        QString mtPtClean = QString::fromLocal8Bit( mtPt );
        mtPtClean.remove( '\x00' );

        mMountPoints << mtPtClean;
    }
}
