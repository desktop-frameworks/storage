# DFL::Storage

The DFL::Storage provides easy access to the attached storage devices. Additionally, it also provides functions to mount/unmount drives.


### Dependencies:
* <tt>Qt5 Core and DBus (qtbase5-dev, qtbase5-dev-tools)</tt>
* <tt>meson   (For configuring the project)</tt>
* <tt>ninja   (To build the project)</tt>


### Notes for compiling - linux:

- Install all the dependencies
- Download the sources
  * Git: `git clone https://gitlab.com/desktop-frameworks/storage.git dfl-storage`
- Enter the `dfl-storage` folder
  * `cd dfl-storage`
- Configure the project - we use meson for project management
  * `meson .build --prefix=/usr --buildtype=release`
- Compile and install - we use ninja
  * `ninja -C .build -k 0 -j $(nproc) && sudo ninja -C .build install`


### Known Bugs
* Please test and let us know


### Upcoming
* Any feature that you ask for.
